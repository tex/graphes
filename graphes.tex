\documentclass[12pt,a4paper]{amsart}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amscd}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{pifont}
\usepackage{alltt}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{setspace}
\usepackage{color}
\usepackage{lscape}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}

\usepackage{tikz-cd}

\pagestyle{empty}


\newcommand{\diff}{\mathrm{d}}
\newcommand{\e}{\mathrm{e}}
\newcommand{\im}{\mathbf{i}}
\newcommand{\jm}{\mathbf{j}}
\newcommand{\red}{\textcolor{red}}
\newcommand{\blue}{\textcolor{blue}}
\newcommand{\green}{\textcolor{green}}
\newcommand{\lensent}{[\![}
\newcommand{\rensent}{]\!]}


\begin{document}

\section*{Rappels sur le calcul matriciel}
\subsection*{Introduction et définition :} \medskip
\paragraph*{\underline{Définition}}
Soient $m$ et $n$ deux entiers naturels non nuls. \\
Une matrice est un tableau de nombres. Si le tableau comporte $m$ lignes et $n$ colonnes, on dit que la matrice est de dimension \textcolor{red}{$m\times n$}.
\medskip
\paragraph*{\underline{Exemple}}
$\begin{pmatrix} 2&0&-1\\5&\frac{1}{3}&4\end{pmatrix}$ est une matrice de dimension 2$\times$3.
\medskip
\paragraph*{\underline{Remarques}}\begin{itemize}
	\item On peut noter les matrices entre parenthèses ou entre crochets.
	\item On parle de \textit{dimension} ou bien de \textit{taille} ou encore de \textit{format}.
\end{itemize}

\medskip
\medskip
\medskip

\paragraph*{\underline{Définition}}
Soit $M$ une matrice de dimension $m\times n$. \begin{itemize}
	\item[$\rightarrow$] Si \textcolor{red}{$m=1$}, on dit que $M$ est une \textit{matrice ligne}.
	\item[$\rightarrow$] Si \textcolor{red}{$n=1$}, on dit que $M$ est une \textit{matrice colonne}.
	\item[$\rightarrow$] Si \textcolor{red}{$m=n$}, on dit que $M$ est une \textit{matrice carrée}.
\end{itemize}

\medskip
\medskip
\medskip

\subsubsection*{Notation générale :}
Les nombres composant une matrice sont ses \textit{coefficients}. \\
Si $A$ est une matrice de dimension $m\times n$, on note : \\ \[\textcolor{red}{\left(a_{ij}\right)_{\substack{1\le i\le m \\ 1\le j\le n}}}\]
où $a_{ij}$ est le coefficient situé à la $i^{\text{ième}}$ ligne et à la $j^{\text{ième}}$ colonne.

\medskip
\medskip

\paragraph*{\underline{Exemple}} Soit $A=\left(a_{ij}\right)_{\substack{1\le i\le 3 \\ 1\le j\le 2}}$ avec $A=\begin{pmatrix} 2&5\\1&4\\3&-1\end{pmatrix}$ \\\\
$\rightarrow\qquad a_{1,2}=5\quad ;\quad a_{3,1}=3\quad ;\quad a_{3,2}=-1$

\medskip
\medskip
\medskip

\subsubsection*{\textbf{Matrices particulières :}}
\begin{itemize}
	\item on appelle \textit{matrice nulle} toute matrice carrée d'ordre $n$ dont tous les coefficents sont nuls : \[\textcolor{red}{O_n}\]
	\item on appelle \textit{matrice identité} d'ordre $n$, notée \textcolor{red}{$I_n$}, la matrice carée d'ordre $n$ dont les coefficients vérifient : \textcolor{red}{
	\[\forall i\in[1,n]\text{ et }\forall j\in[1,n],\begin{cases} \text{si } i=j,\quad a_{ij}=1 \\
																  \text{si } i\neq j,\quad a_{ij}=0 \end{cases}\]}
\end{itemize}

\paragraph*{\underline{Exemples}}

\[I_3=\begin{pmatrix}
1&0&0 \\ 0&1&0 \\ 0&0&1
\end{pmatrix} \qquad I_2=\begin{pmatrix}
1&0 \\ 0&1
\end{pmatrix}\]

\newpage

\subsection*{Opérations sur les matrices :}
\subsubsection*{\textbf{Addition et multiplication par un réel :}} \medskip
\paragraph*{\underline{Définition}} Soient $A$ et $B$ deux matrices de même dimension $m\times n$ avec $A=\left(a_{ij}\right)_{\substack{1\le i\le m \\ 1\le j\le n}}$ et $B=\left(B_{ij}\right)_{\substack{1\le i\le m \\ 1\le j\le n}}$ \\\\

La matrice somme $A+B$ est la matrice $\left(c_{ij}\right)_{\substack{1\le i\le m \\ 1\le j\le n}}$ telle que, $\forall i\in[1,m]$ et $\forall j\in[1,n]$ : \textcolor{red}{\\
\[c_{ij}=a_{ij}+b_{ij}\]
} \medskip
Pour additionner deux matrices de même dimension, on ajoute simplement les coefficients situés à la même place (vulgarisation).

\medskip
\medskip

\paragraph*{\underline{Exemple}}
\begin{equation*}
\begin{pmatrix}	2&3&-1\\0&5&\dfrac{1}{3}\end{pmatrix}+\begin{pmatrix} -5&3&-2\\1&0&\dfrac{2}{3}\end{pmatrix}=\begin{pmatrix} -1&6&-3\\1&5&1\end{pmatrix}
\end{equation*}
\medskip
\paragraph*{\underline{Remarques}}\begin{itemize}
	\item L'addition de matrices est \textit{commutative} : $A+B=B+A$
	\item Si $A$ est carrée d'ordre $n$ et que $O_n$ est la matrice nulle d'ordre $n$, alors : $A+O_n=A$
\end{itemize}

\medskip
\medskip
\medskip

\paragraph*{\underline{Définition}} Soit $A$ une matrice $m\times n$, avec $A=\left(a_{ij}\right)_{\substack{1\le i\le m \\ 1\le j\le n}}$ \\
Soit $\lambda$ un réel. On appelle produit de $\lambda$ par $A$ la matrice \textcolor{red}{$A=\left(b_{ij}\right)_{\substack{1\le i\le m \\ 1\le j\le n}}$} telle que, $\forall i\in[1,m]$ et $\forall j\in[1,n]$ : \textcolor{red}{\\
\[b_{ij}=\lambda\cdot a_{ij}\]
} \medskip
Pour multiplier deux matrices par un nombre, on multiplie chaque coefficient de la matrice par ce nombre (vulgarisation).

\medskip
\medskip

\paragraph*{\underline{Exemple}}
\begin{equation*}
A=\begin{pmatrix} 2&-1\\5&4 \end{pmatrix} \qquad\rightarrow\qquad -2A=\begin{pmatrix} -4&2\\-10&-8\end{pmatrix}
\end{equation*}
\medskip
\paragraph*{\underline{Remarque}} $A$ et $B$ sont deux matrices de mêmes dimensions, $\lambda$ et $\eta$ sont deux réels : \begin{itemize}
	\item[$\rightarrow$] $\lambda(\eta\cdot A)=\lambda\cdot\eta\cdot A$
	\item[$\rightarrow$] $\lambda(A+B)=\lambda\cdot A+\lambda\cdot B$
\end{itemize}

\medskip
\medskip
\medskip

\paragraph*{\underline{Définition}} On peut définir la soustraction de la matrice $A$ par $B$, deux matrices de mêmes dimensions, par :
\[A-B=A+(-1)\cdot B\]

\newpage

\subsubsection*{\textbf{Multiplication de matrices :}}
\paragraph*{\underline{Définition}} Soit $A$ une matrice ligne de dimension $1\times n$ et $B$ une matrice colonne de dimension $n\times1$. \\
Alors, on définit le produit $A\times B$ de matrices par le calcul suivant : \textcolor{red}{
\[A\times B=\begin{pmatrix}a_{1,1}&a_{1,2}&\dots&a_{1,n}\end{pmatrix}\times\begin{pmatrix}b_{1,1}\\b_{2,1}\\ \vdots\\b_{n,1}\end{pmatrix}=a_{1,1}b_{1,1}+a_{1,2}b_{2,1}+\dots+a_{1,n}b_{n,1}\]
}

\medskip
\medskip

\paragraph*{\underline{Exemple}}
\begin{equation*}
\begin{pmatrix}1&4&-3\end{pmatrix}\times\begin{pmatrix}-2\\1\\-1 \end{pmatrix}=4
\end{equation*}

\bigskip

\paragraph*{\underline{Définition}}
Soit $A$ une matrice de dimension $m\times \blue{n}$ et $B$ une matrice de dimension $\blue{n}\times p$. \\
Le produit de $A\times B$ (et non $B\times A$ !!) est la matrice de dimension $\red{m\times p}$ dont le coefficient placé à la $i^{\text{ème}}$ ligne et à la $j^{\text{ième}}$ colonne est égal au produit de la matrice ligne $i^{\text{ème}}$ de $A$ par la $j^{\text{ème}}$ matrice colonne de $B$ au sens du produit définit précédemment.
\bigskip
\paragraph*{\underline{Exemple}}
\begin{equation*}
\begin{pmatrix}2&3&5\\1&1&-1\end{pmatrix}\times\begin{pmatrix}3&-1&0\\0&5&1\\1&4&0\end{pmatrix}=\begin{pmatrix}11&33&3\\2&0&1\end{pmatrix}
\end{equation*}
\bigskip
\bigskip

\subsubsection*{\textbf{Propriétés sur les matrices :}}
Soient $A,B,C$ trois matrices carrées d'ordre $n$. \\ \begin{itemize}
	\item[$\rightarrow$] Le produit est \textit{associatif} : \[(A\times B)\times C=A\times(B\times C)\]
	\item[$\rightarrow$] Le produit est distributif par rapport à l'addition : \[A\times(B+C)=A\times B+A\times C\qquad(\neq B\times A+C\times A)\]
	\item[$\rightarrow$] Le produit par un réel, $\lambda\in\mathbb{R}$ : \begin{equation*}\begin{split}
																								& (\lambda\cdot A)\times B=\lambda\cdot(A\times B) \\
																								\text{et } & A\times(\lambda\cdot B)=\lambda\cdot(A\times B)
																							\end{split}
																		  \end{equation*}
	\item[$\rightarrow$] L'élément neutre : $I_n$ \[A\times I_n=I_n\times A=A\]
\end{itemize}
\bigskip
\paragraph*{\underline{Remarque}} En règle générale, \red{$A\times B\neq B\times A$} \\
\begin{equation*}
	\begin{split}
		& \begin{pmatrix}2&1\\4&2\end{pmatrix}\times\begin{pmatrix}4&-2\\2&1\end{pmatrix}=\begin{pmatrix}10&-3\\20&-6\end{pmatrix} \\
		\text{mais : } & \begin{pmatrix}4&-2\\2&1\end{pmatrix}\times\begin{pmatrix}2&1\\4&2\end{pmatrix}=\begin{pmatrix}0&0\\8&4\end{pmatrix}
	\end{split}
\end{equation*}

\newpage

\paragraph*{\underline{Définition}} Soit $A$ une matrice carrée d'ordre $n$. \\
On appelle carrée de la matrice, et on note \red{$A^2$}, la matrice $\red{A^2=A\times A}$. \\\\
\medskip
Ainsi, on définit \red{$A^m=\underbrace{A\times A\times\dots\times A}_{m\text{ fois}}$} \\\\
\medskip
On convient que \red{$A^0=I_n$} \\\\

\paragraph*{\underline{Exemple}} $A\begin{pmatrix}2&-3\\-1&5\end{pmatrix}$
\begin{equation*}
	A^2=\begin{pmatrix}2&-3\\-1&5\end{pmatrix}\begin{pmatrix}2&-3\\-1&5\end{pmatrix}=\begin{pmatrix}7&-21\\-7&28\end{pmatrix}
\end{equation*}
\bigskip

\paragraph*{\underline{Remarque}}
\begin{equation*}
\begin{split}(A+B)^2 &=(A+B)(A+B)\\
					 &=A^2+AB+BA+B^2\quad\red{\neq A^2+2AB+B^2}
\end{split}
\end{equation*} \\ \medskip
D'où : $(A-B)(A+B)\neq A^2-B^2$, en général.

\bigskip
\bigskip

\subsection*{Matrices inversibles et systèmes :}
\paragraph*{\underline{Définition}} Soit $A$ une matrice carrée d'ordre $n$. \\
On dit que $A$ est inversible s'il existe une matrice, notée \red{$A^{-1}$}, telle que : \red{\[A\cdot A^{-1}=A^{-1}\cdot A=I_n\] }

\medskip

\paragraph*{\underline{Remarque}} Sur $\mathbb{R}$, le seul élément non-inversible est $0$. \\
Concernant les matrices, il y a une infinité de matrices non-inversibles. \\

\bigskip
\bigskip

\subsubsection*{\textbf{Propriétés :}} \begin{itemize}
	\item Si $A$ est inversible, alors $A^{-1}$ est \underline{unique}.
	\item L'inverse d'une matrice carrée d'ordre 2 : \\
Soit $A=\begin{pmatrix}a&b\\c&d\end{pmatrix}$ \\
$A$ est inversible ssi \red{$ad-bc\neq 0$} :\\ \red{
\[\fbox{$A^{-1}\dfrac{1}{ad-bc}\begin{pmatrix}d&-b\\-c&a\end{pmatrix}$}\]
}
\end{itemize} \medskip

\paragraph*{\underline{Exemple}}
$A\begin{pmatrix}-3&-2\\5&4\end{pmatrix}$ \begin{itemize}
	\item[$\rightarrow$] $-3\times4-5\times(-2)=-2\neq0$
	\item[$\rightarrow$] $A^{-1}=\dfrac{1}{-2}\begin{pmatrix}4&2\\-5&-3\end{pmatrix}$
	\item[$\rightarrow$] $A^{-1}=\begin{pmatrix}-2&-1\\ \frac{5}{2}&\frac{3}{2}\end{pmatrix}$
\end{itemize} \medskip

Les matrices, grâce à leur inverses, permettent de résoudre des systèmes à $n$ inconnues et à $n$ équations. \\ \medskip

\paragraph*{\underline{Exemple}}
\begin{equation*}
\begin{tabular}{|l}
	2a-3b+4c=-1 \\
	a+b-5c=2 \\
	-4a+b-6=0 \\
\end{tabular}
\end{equation*} \\\\
\medskip
$\rightarrow$ Le système équivaut matriciellement à :
\begin{equation*}
\underbrace{\begin{pmatrix}2&-3&4\\1&1&-5\\-4&1&0\end{pmatrix}}_A\underbrace{\begin{pmatrix}a\\b\\c\end{pmatrix}}_X=\underbrace{\begin{pmatrix}-1\\2\\6\end{pmatrix}}_B
\end{equation*}
On a :
\begin{gather*}
	AX=B\;\Longleftrightarrow\;X=A^{-1}B \\
	A^{-1}B=\begin{pmatrix}-23/10\\-16/5\\3/2\end{pmatrix} \\
	(a,b,c)=(-2.3,-3.2,1.5)
\end{gather*}

\newpage

\subsubsection*{\textbf{Propriétés :}} Un système de $n$ équations à $n$ inconnues qui s'écrit : \\ \medskip
\begin{equation*}
	\left|\begin{split}
			a_{1,1}x_1 &+a_{1,2}x_2 &+\dots &+a_{1,n}x_n &=b_1 \\
			a_{2,1}x_1 &+a_{2,2}x_2 &+\dots &+a_{2,n}x_n &=b_2 \\
			\vdots & &\vdots & &\vdots \\
			\vdots & &\vdots & &\vdots \\
			\vdots & &\vdots & &\vdots \\
			a_{n,1}x_1 &+a_{n,2}x_2 &+\dots &+a_{n,n}x_n &=b_n
		  \end{split}\right.
\end{equation*} \\ \\
\medskip
Où $a_{ij}\in\mathbb{R}$, $x_i$ sont les inconnues, et $b_i\in\mathbb{R}$, \\
Se traduit matriciellement par $AX=B$ :
\[A\begin{pmatrix}
a_{1,1}&a_{1,2}&\dots&a_{1,n}\\ \vdots&\vdots&\vdots&\vdots\\a_{n,1}&a_{n,2}&\dots&a_{n,n}\end{pmatrix}\quad ;\quad
X\begin{pmatrix}x_1\\x_2\\ \vdots\\x_n\end{pmatrix}\quad \text{et}\quad
B\begin{pmatrix}b_1\\b_2\\ \vdots\\b_n\end{pmatrix}\]

et peut se résoudre : si $A$ est inversible, alors : \begin{equation*}
\begin{split}
	AX=B\quad\Longleftrightarrow\quad A^{-1}AX &=A^{-1}B \\
	X &=A^{-1}B
\end{split}
\end{equation*} 

\newpage

\section*{Graphes et Matrices}
\subsection*{Vocabulaire des graphes non-orientés :}
\paragraph*{\underline{Définition}}
Un graphe est constitué de deux ensembles finis, l'un de \textit{sommets}, l'autre d'\textit{arrêtes}. Les arrêtes sont représentées graphiquement par des traits simples, et on trouve à leur extrémités les sommets. \\\\
\paragraph*{\underline{Remarques}} \begin{itemize}
	\item Une arrête qui a le même sommet à ses deux extrémités est une \textit{boucle}.
	\item Les arrêtes peuvent être multiples.
\end{itemize} \bigskip

\paragraph*{\underline{Exemples}} $\text{ }$ \\
\begin{center}
\begin{tabular}{cc}

\begin{tikzcd}
A \ar[loop left, dash] \ar[r, shift left, dash] \ar[r, dash] \ar[r, shift right, dash] &B \\
\end{tikzcd} \\

\begin{tikzcd}
	& \ar[dl, dash, red] 1 \ar[dr, dash, blue] & \\
	2 \ar[rr, dash, "x"' green] && 3 \\
\end{tikzcd}
\end{tabular}
\end{center}

\bigskip
\bigskip

\paragraph*{\underline{Définition}}
Le \textit{degré} d'un sommet est le nombre d'extrémités qui lui sont rattachées (une boucle comptera pour 2). \\
On peut en déduire que \textbf{la somme de degrés d'un graphe est égale au double de son nombre d'arrêtes}. \\
Lorsque que deux sommets sont reliés par au moins une arrête, ils sont alors dit \textit{adjacents}, ou voisins. On peut alors à partir de là former ce que l'on appelle des \textit{chaines} : l'ensemble de $n$ sommets adjacents forme une chaine de longueur $n$. \\
Une chaine est dite \textit{eulerienne} lorsque toutes les arrêtes du graphes y apparaissent au moins et au plus une fois. \\
Enfin, le nombre chromatique d’un graphe $G$, noté $\chi(G)$, est le nombre de couleurs nécessaires pour colorier chaque sommet, sachant que deux voisins ne peuvent avoir la même couleur.

\bigskip
\bigskip


\end{document}